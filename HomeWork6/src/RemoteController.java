public class RemoteController {
private TV tv;
private String name;

    public RemoteController(String name) {
        this.name = name;
    }
    public void takeTheSignal(TV tv) {
        this.tv = tv;
        this.tv.takeSignalFromRemoteController(this);
    }
    public void run() {
        this.tv.run();
    }

}
