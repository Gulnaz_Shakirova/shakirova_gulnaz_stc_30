import java.util.Scanner;

public class TV {
    private String name;
    private RemoteController remoteController;
    private Channel channels[];
    private int channelsCount;
    private int currentChannel;
    Scanner input = new Scanner(System.in);

    public TV (String name) {
        this.name = name;
        this.channels = new Channel[5];
    }

    public void takeSignalFromRemoteController(RemoteController remoteController) {
        this.remoteController = remoteController;
    }

    public void takeChanel (Channel channel) {
        this.channels[channelsCount] = channel;
        this.channelsCount++;
    }
    public void run() {
        System.out.println("TV "+this.name+" is on");
        for (int i = 0; i < channelsCount; i++) {
            System.out.println(channels[i].getName()+" is on");
        }
    }
    public void changeChannel (int numberOfChannel) {
        System.out.println("Find the " + numberOfChannel + " channel");
        numberOfChannel = input.nextInt();
        for(int i = 0; i < channelsCount; i++) {
            if (channels[i].getNumber() == numberOfChannel) {
                System.out.println(channels[i].getName());
                currentChannel = i;
                break;
            }
        }
    }
    public Channel getCurrentChannel () {
        return channels[currentChannel];
    }
}
