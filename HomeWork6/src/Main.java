public class Main {

    public static void main(String[] args) {
        TV tv = new TV("Цветочек");
        Channel c1 = new Channel("Первый", 1);
        Channel c2 = new Channel("Россия", 2);
        Channel c3 = new Channel("СТС", 3);
        Channel c4 = new Channel("Культура", 4);
        Channel c5 = new Channel("ТНТ", 5);

        Program p1 = new Program("MythBusters");
        Program p2 = new Program("ЗдоровоЖить");
        Program p3 = new Program("Доктор Кто");
        Program p4 = new Program("Мстители");
        Program p5 = new Program("Галилео");

        p1.getName();
        p2.getName();
        p3.getName();
        p4.getName();
        p5.getName();

        System.out.println(p1.getName());

        Program programs[] = new Program[5];
        programs[0] = p1;
        programs[1] = p2;
        programs[2] = p3;
        programs[3] = p4;
        programs[4] = p5;


        Channel channels[] = new Channel[5];
        channels[0] = c1;
        channels[1] = c2;
        channels[2] = c3;
        channels[3] = c4;
        channels[4] = c5;
        for (int i = 0; i < channels.length; i++) {
            channels[i].IntProgram(programs[i]);
            tv.takeChanel(channels[i]);
        }

        tv.run();
        tv.changeChannel(2);
        tv.getCurrentChannel().takeProgram();
    }
}