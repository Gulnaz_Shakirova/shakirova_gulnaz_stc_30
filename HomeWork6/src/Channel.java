import java.util.Random;

public class Channel {
    private TV tv;
    private String name;
    private Program programs[];
    private int programsCount;
    private int number;

    public Channel(String name, int number) {
        this.name = name;
        this.programs = new Program[10];
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public void IntProgram(Program program) {
        this.programs[programsCount] = program;
        this.programsCount++;
    }

    public String getName() {
        return name;
    }

    public void takeProgram() {
        Random random = new Random();
        Program program = programs[random.nextInt(programsCount)];
        System.out.println(program.getName());
    }
}
