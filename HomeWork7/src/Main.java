public class Main {

    public static void main(String[] args) {
	Person person = new Person.Builder()
            .firstName("Гульназ")
	        .lastName("Шакирова")
	        .age(29)
	        .isFinishedCourse(false)
            .build();
	System.out.println(person.getName());
    }
}
