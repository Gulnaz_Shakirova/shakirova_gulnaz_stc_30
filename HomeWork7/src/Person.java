public class Person {
    private String name;
    private String surname;
    private int years;
    private boolean isFinishing;

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getYears() {
        return years;
    }

    public boolean isFinishing() {
        return isFinishing;
    }

    public static class Builder {
        private Person newPerson;

        public Builder() {
            newPerson = new Person();
        }
        public Builder firstName(String name) {
            newPerson.name = name;
            return this;
        }
        public Builder lastName(String surname) {
            newPerson.surname = surname;
            return this;
        }
        public Builder age(int years) {
            newPerson.years = years;
            return this;
        }
        public Builder isFinishedCourse(boolean isFinishing) {
            newPerson.isFinishing = isFinishing;
            return this;
        }
        public Person build() {
            return newPerson;
        }
    }
}
