public interface Collection<C> extends Iterable<C> {
    void add(C element);
    boolean contains(C element);
    int size();
    boolean removeFirst(C element);
}
