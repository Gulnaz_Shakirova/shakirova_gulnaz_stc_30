public class LinkedList<E> implements List<E> {

    private class LinkedListIterator implements Iterator<E> {

        @Override
        public E next() {
            return null;
        }

        @Override
        public boolean hasNext() {
            return false;
        }
    }

    private Node<E> first;
    private Node<E> last;

    private int count;

    private static class Node<F> {
        F value;
        Node<F> next;
        Node<F> previous;

        public Node(F value) {
            this.value = value;
        }
    }

    @Override
    public E get(int index) {
        if (index >= 0 && index < count && first != null) {
            int i = 0;
            Node<E> current = this.first;

            while (i < index) {
                current = current.next;
                i++;
            }

            return current.value;
        }
        System.err.println("Такого элемента нет");
        return null;


    }

    @Override
    public int indexOf(E element) {
        int i = 0;
        Node<E> current = this.first;

        while (current != null && current.value != element) {
            current = current.next;
            i++;
        }

        if (current == null) {
            return -1;
        } else {
            return i;
        }
    }

    @Override
    public boolean removeByIndex(int index) {
        if (index < 0 || index >= count) {
            throw new IllegalArgumentException();
        }
        return false;
    }

    @Override
    public void insert(E element, int index) {
        if (index < 0 || index >= count) {
            throw new IllegalArgumentException();
        }
        LinkedList.Node newNode = new LinkedList.Node(element);
        if (index == 0) {
            add(element);
        }
        if (index == count) {
            last.next = newNode;
            last = newNode;
        }
        LinkedList.Node oldNode = first;
        for (int i = 0; i < index; i++) {
            oldNode = oldNode.next;
        }
        LinkedList.Node oldPrevious = oldNode.previous;
        if (oldPrevious != null) {
            oldPrevious.next = newNode;
        }
        oldNode.previous = newNode;

        newNode.previous = oldPrevious;
        newNode.next = oldNode;
        count++;
    }

    @Override
    public void add(E element) {
        Node<E> newNode = new Node<E>(element);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
            last.next = newNode;
            last = newNode;
        }
        count++;
    }

    @Override
    public boolean contains(E element) {
        for (int i = 0; i < count; i++) {
            if(get(i) == element) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public boolean removeFirst(E element) {
        Node<E> nodeBefore = findNodeBefore(element);
        if (nodeBefore != null) {
            if (nodeBefore.value == null) {
                first = first.next;
                count--;
                return true;
            }
        }
        return false;
    }

    private Node<E> findNodeBefore(E value) {
        if (first.value == value) {
            return new Node<E>(value);
        }

        Node<E> node = first;
        while (node.next != null) {
            if (node.next.value == value) {
                return node;
            }
            node = node.next;
        }
        return null;
    }

    @Override
    public Iterator iterator() {
        return new LinkedListIterator();
    }

    @Override
    public void reverse() {
        Node<E> previousNode = null;
        Node<E> currentNode = first;
        while (currentNode != null) {
            Node<E> nextNode = currentNode.next;
            currentNode.next = previousNode;
            previousNode = currentNode;
            currentNode = nextNode;
        }
        first = previousNode;
    }
}
