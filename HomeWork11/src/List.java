public interface List<D> extends Collection<D> {
    D get(int index);
    int indexOf(D element);
    boolean removeByIndex(int index);
    void insert(D element, int index);
    void reverse();
}
