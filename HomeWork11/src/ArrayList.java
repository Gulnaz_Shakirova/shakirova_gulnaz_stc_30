public class ArrayList<G> implements List<G> {
    private static final int DEFAULT_SIZE = 10;
    private G data[];
    private int count;


    public ArrayList() {
        this.data = (G[]) new Object[DEFAULT_SIZE];
    }

    private class ArrayListIterator implements Iterator<G> {
        private int current = 0;

        @Override
        public G next() {
            G value = data[current];
            current++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current < count;
        }
    }

    @Override
    public G get(int index) {
        if (index < count) {
            return this.data[index];
        }
        System.err.println("Вышли за пределы массива");
        return null;
    }

    @Override
    public int indexOf(G element) {
        for (int i = 0; i < count; i++) {
            if (data[i] == element) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public boolean removeByIndex(int index) {
        if (index < 0 || index >= count) {
            throw new IllegalArgumentException();
        }
        return false;
    }

    @Override
    public void insert(G element, int index) {
        data[index] = null;
        count++;
        if (count > data.length -1) {
            icreasingTheArray();
        }
    }

    private void icreasingTheArray() {
        G[] newArray = (G[]) new Object[data.length *2];
        System.arraycopy(data, 0, newArray, 0, count);
        data = newArray;
    }

    @Override
    public void reverse() {
        for (int i = 0; i < data.length /2; i++)
        {
            G First = data[i];
            data[i] = data[count - 1 - i];
            data[count - 1 - i] = First;
        }
    }

    @Override
    public boolean contains(G element) {
        return indexOf(element) != -1;
    }

    @Override
    public void add(G element) {
        if (count == data.length - 1) {
            resize();
        }
        data[count] = element;
        count++;
    }

    private void resize() {
        int oldLength = this.data.length;
        int newLength = oldLength + (oldLength >> 1);
        G newData[] = (G[]) new Object[newLength];
        System.arraycopy(this.data, 0, newData, 0, oldLength);
        this.data = newData;
    }

    @Override
    public int size() {
        return this.count;
    }

    @Override
    public boolean removeFirst(G element) {
        int indexOfRemovingElement = indexOf(element);

        for (int i = indexOfRemovingElement; i < count - 1; i++) {
            this.data[i] = this.data[i + 1];
        }

        this.count--;
        return false;
    }

    @Override
    public Iterator<G> iterator() {
        return new ArrayListIterator();
    }
}
