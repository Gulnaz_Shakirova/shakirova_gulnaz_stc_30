import java.util.Scanner;
import java.util.Arrays;
public class HW3_1 {
	public static int SumArray (int a[], int ArraySum) {
		for (int i = 0; i < a.length; i++) {
			ArraySum = ArraySum + a[i];
		}
	return ArraySum;
	}
	public static void ArrayTurn (int a[], int First, int size) {
		for (int i = 0; i < a.length/2; i++) {
			First = a [i];
			a [i] = a [size - 1 - i];
			a [size - 1 - i] = First;
		}
	}
	public static int Average (int a[], int Average, int sum) {
		for (int i = 1; i < a.length; i++) {
			sum = sum + a[i];
		}
		Average = sum / a.length;
		return Average;
	}
	public static void swapInArray (int a[], int IndexOfMax, int IndexOfMin) {
		int temp = a [IndexOfMax];
		a [IndexOfMax] = a [IndexOfMin];
		a [IndexOfMin] = temp;
	}
	public static void BubbleSort (int a[]) {
		for (int i = a.length; i > 0; i--) {
			for (int j = 0; j < i-1; j++) {
				if (a[j] > a[j+1]) {
					int temp = a[j];
					a[j] = a[j+1];
					a[j+1] = temp;
				}
			}
		}
	}
	
	public static void length (int a[]) {
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i]);
		}
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner (System.in);
		int size = scanner.nextInt();
		int [] a = new int [size];
		int ArraySum = 0;
		int First = a[0];
		int Average = 0;
		int sum = 0;
		
		for (int i = 0; i < a.length; i++) {
			a [i] = scanner.nextInt();
		}	
	System.out.println(Arrays.toString(a));

	ArraySum = SumArray (a, ArraySum);
	System.out.println("Sum of numbers: " + ArraySum);
	
	ArrayTurn(a, First, size);
	System.out.println("Turn of Array: ");
	System.out.println(Arrays.toString(a));

	Average = Average(a, Average, sum);
	System.out.println("Average: " + Average);

	int max = a [0];
	int IndexOfMax = 0;
	for (int i = 0; i < a.length; i++) {
			if(max < a[i]) {
				max = Math.max (max, a[i]);
				IndexOfMax = i;
			}
		}	
		int min = a[0];
		int IndexOfMin = 0;
		for (int j = 0; j < a.length; j++) {
			if(min > a[j]) {
				min = Math.min (min, a[j]);
				IndexOfMin = j;
				}
		}	

	System.out.println("Index of max: " + IndexOfMax);
	System.out.println("Index of min: " + IndexOfMin);

	swapInArray(a, IndexOfMax, IndexOfMin);
	System.out.println("Array with changed mix and max: ");
	System.out.println(Arrays.toString(a));
	
	BubbleSort(a);
	System.out.println("Array sorted: ");
	System.out.println(Arrays.toString(a));
	
	length(a);
	}
}