public class Circle extends GeometricFigure {
    private int radius;
    private int Perimetr;
    private int Square;

    public Circle(int radius) {
        super(0, 0);
        this.radius = radius;
    }

    @Override
    public void perimetrToCount() {
        Perimetr = (int) (2*Math.PI*radius);
        System.out.println("Периметр круга: " + Perimetr);
    }
    @Override
    public void squareToCount() {
        Square = (int) (Math.PI*radius*radius);
        System.out.println("Площадь круга: " + Square);
    }
}
