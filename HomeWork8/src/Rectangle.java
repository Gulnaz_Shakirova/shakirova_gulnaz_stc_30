public class Rectangle extends GeometricFigure {
    private int side1;
    private int side2;
    private int Perimetr;
    private int Square;

    public Rectangle(int side1, int side2) {
        super(0, 0);
        this.side1 = side1;
        this.side2 = side2;
    }

    @Override
    public void perimetrToCount() {
        Perimetr = 2*(side1+side2);
        System.out.println("Периметр прямоугольника: " + Perimetr);
    }
    @Override
    public void squareToCount() {
        Square = side1*side2;
        System.out.println("Площадь прямоугольника: " + Square);
    }
}
