public class Main {

    public static void main(String[] args) {
    	final int side1 = 2;
    	int side2 = 3;

		GeometricFigure g1 = new Ellipsoid(10, 5);
		GeometricFigure g2 = new Circle(4);
		GeometricFigure g3 = new Rectangle(7, 9);
		GeometricFigure g4 = new Square(3);

		GeometricFigure geometricFigures[] = {g1, g2, g3, g4};
		for (int i = 0; i < geometricFigures.length; i++) {
			geometricFigures[i].perimetrToCount();
			geometricFigures[i].squareToCount();
		}
		GeometricFigure geometricFigure = new GeometricFigure(3,7) {
			@Override
			public void perimetrToCount() {
			}

			@Override
			public void squareToCount() {
			}
		};
		geometricFigure.relocationOfFigure();
		geometricFigure.scalingOfFigure();

		Scalable scalable = () -> {
			System.out.println("Увеличение масштаба фигур: " + (side1*side2*2));
		};
	}
}
