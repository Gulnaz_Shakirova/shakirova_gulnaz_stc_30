public abstract class GeometricFigure implements Scalable, Relocatable {
    private int side1;
    private int side2;

    public GeometricFigure(int side1, int side2) {
        this.side1 = side1;
        this.side2 = side2;
    }

    public abstract void perimetrToCount();

    public abstract void squareToCount();

    public void scalingOfFigure() {
        System.out.println("Увеличение масштаба фигур: " + (side1*side2*2));
    }

    public void relocationOfFigure () {
        side1 = side1-1;
        side2 = side2+1;
        System.out.println("Смещение фигур влево: " + side1 + "," + side2);
    }
}
