public class Ellipsoid extends GeometricFigure {
    private int axis1;
    private int axis2;
    private int Perimetr;
    private int Square;
    private String typeOfFigure = "Эллипс";

    public Ellipsoid(int axis1, int axis2) {
        super(0, 0);
        this.axis1 = axis1;
        this.axis2 = axis2;
    }
    @Override
    public void perimetrToCount() {
        Perimetr = (int) (2*Math.PI*Math.sqrt((axis1*axis1+axis2*axis2)/8));
        System.out.println("Периметр эллипса: " + Perimetr);
    }
    @Override
    public void squareToCount() {
        Square = (int) (Math.PI*axis1*axis2);
        System.out.println("Площадь эллипса: " + Square);
    }

}
