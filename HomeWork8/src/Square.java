public class Square extends GeometricFigure {
    private int side1;
    private double Perimetr;
    private double Square;

    public Square(int side1) {
        super(0, 0);
        this.side1 = side1;
    }

    @Override
    public void perimetrToCount() {
        Perimetr = 4*side1;
        System.out.println("Периметр квадрата: " + Perimetr);
    }
    @Override
    public void squareToCount() {
        Square = side1*side1;
        System.out.println("Площадь квадрата: " + Square);
    }
}
