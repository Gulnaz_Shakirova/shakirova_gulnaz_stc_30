import java.util.Scanner;
import java.util.Arrays;
public class HW4_2 {

	public static int binarySearch(int [] a, int first, int last, int item) {
	int mid = (first + last) / 2;
	if(last < first) {
		return -1;
	}
	if(a[mid] == item) {
		return mid;
	} else if(a[mid] > item) {
		return binarySearch(a, first, mid - 1, item);
		} else {
		return binarySearch(a, mid + 1, last, item);
		}
	}
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner (System.in);
		int size = scanner.nextInt();
		int [] a = new int [size];
		int item;
		int first = 0;
		int last = size-1;
		int mid = (first + last) / 2;
	
	for (int i = 0; i < a.length; i++) {
		a [i] = scanner.nextInt();		
	}
	System.out.println(Arrays.toString(a));
	Arrays.sort(a);
	System.out.println(Arrays.toString(a));
	
	System.out.println("Enter number for search: ");
	item = scanner.nextInt();
	
	binarySearch(a, first, last, item);
	if(first <= last) {
		System.out.println(item + " is the " + mid + " element in Array");
	} else {
		System.out.println("Element is not found");
	}
	}
	}
