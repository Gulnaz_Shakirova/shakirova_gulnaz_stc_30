public interface Collection extends Iterable {
    void add(int element);
    boolean contains(int element);
    int size();
    boolean removeFirst(int element);
}
