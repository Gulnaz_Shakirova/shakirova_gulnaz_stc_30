public class ArrayList implements List {
    private static final int DEFAULT_SIZE = 10;
    private int data[];
    private int count;


    public ArrayList() {
        this.data = new int[DEFAULT_SIZE];
    }

    private class ArrayListIterator implements Iterator {
        private int current = 0;

        @Override
        public int next() {
            int value = data[current];
            current++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current < count;
        }
    }

    @Override
    public int get(int index) {
        if (index < count) {
            return this.data[index];
        }
        System.err.println("Вышли за пределы массива");
        return -1;
    }

    @Override
    public int indexOf(int element) {
        for (int i = 0; i < count; i++) {
            if (data[i] == element) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public boolean removeByIndex(int index) {
        if (index < 0 || index >= count) {
            throw new IllegalArgumentException();
        }
        return false;
    }

    @Override
    public void insert(int element, int index) {
        data[index] = 0;
        count++;
        if (count > data.length -1) {
            icreasingTheArray();
        }
    }

    private void icreasingTheArray() {
        int[] newArray = new int[data.length *2];
        System.arraycopy(data, 0, newArray, 0, count);
        data = newArray;
    }

    @Override
    public void reverse() {
    }

    @Override
    public boolean contains(int element) {
        return indexOf(element) != -1;
    }

    @Override
    public void add(int element) {
        if (count == data.length - 1) {
            resize();
        }
        data[count] = element;
        count++;
    }

    private void resize() {
        int oldLength = this.data.length;
        int newLength = oldLength + (oldLength >> 1); // oldLength + oldLength / 2;
        int newData[] = new int[newLength];
        System.arraycopy(this.data, 0, newData, 0, oldLength);
        this.data = newData;
    }

    @Override
    public int size() {
        return this.count;
    }

    @Override
    public boolean removeFirst(int element) {
        int indexOfRemovingElement = indexOf(element);

        for (int i = indexOfRemovingElement; i < count - 1; i++) {
            this.data[i] = this.data[i + 1];
        }

        this.count--;
        return false;
    }

    @Override
    public Iterator iterator() {
        return new ArrayListIterator();
    }
}
