import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        testList(new LinkedList());
        testList(new ArrayList());
    }
    public static void testList(List list) {
        for (int i = 0; i < 17; i++) {
            list.add(i);
        }

        list.removeFirst(8);
        System.out.println(list.get(8));
        System.out.println(list.indexOf(11));
        System.out.println(list.size());
        System.out.println(list.contains(15));
        System.out.println(list.removeByIndex(3));
        list.insert(1,3);
        System.out.println();
        list.reverse();

        Iterator iterator = list.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
