public class LinkedList implements List {

    private class LinkedListIterator implements Iterator {

        @Override
        public int next() {
            return 0;
        }

        @Override
        public boolean hasNext() {
            return false;
        }
    }

    private Node first;
    private Node last;

    private int count;

    private static class Node {
        int value;
        Node next;
        Node previous;

        public Node(int value) {
            this.value = value;
        }
    }

    @Override
    public int get(int index) {
        if (index >= 0 && index < count && first != null) {
            int i = 0;
            Node current = this.first;

            while (i < index) {
                current = current.next;
                i++;
            }

            return current.value;
        }
        System.err.println("Такого элемента нет");
        return -1;


    }

    @Override
    public int indexOf(int element) {
        int i = 0;
        Node current = this.first;

        while (current != null && current.value != element) {
            current = current.next;
            i++;
        }

        if (current == null) {
            return -1;
        } else {
            return i;
        }
    }

    @Override
    public boolean removeByIndex(int index) {
    if (index < 0 || index >= count) {
        throw new IllegalArgumentException();
    }
        return false;
}

    @Override
    public void insert(int element, int index) {
        if (index < 0 || index >= count) {
            throw new IllegalArgumentException();
        }
        LinkedList.Node newNode = new LinkedList.Node(element);
        if (index == 0) {
            add(element);
        }
        if (index == count) {
            last.next = newNode;
            last = newNode;
        }
        LinkedList.Node oldNode = first;
        for (int i = 0; i < index; i++) {
            oldNode = oldNode.next;
        }
        LinkedList.Node oldPrevious = oldNode.previous;
        if (oldPrevious != null) {
        oldPrevious.next = newNode;
        }
        oldNode.previous = newNode;

        newNode.previous = oldPrevious;
        newNode.next = oldNode;
        count++;
    }

    @Override
    public void add(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
            last.next = newNode;
            last = newNode;
        }
        count++;
    }

    @Override
    public boolean contains(int element) {
        for (int i = 0; i < count; i++) {
            if(get(i) == element) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public boolean removeFirst(int element) {
        Node nodeBefore = findNodeBefore(element);
        if (nodeBefore != null) {
            if (nodeBefore.value == 0) {
                first = first.next;
                count--;
                return true;
            }
        }
            return false;
        }

    private Node findNodeBefore(int value) {
        if (first.value == value) {
            return new Node(value);
        }

        Node node = first;
        while (node.next != null) {
            if (node.next.value == value) {
                return node;
            }
            node = node.next;
        }
        return null;
    }

    @Override
    public Iterator iterator() {
        return new LinkedListIterator();
    }

    @Override
    public void reverse() {
        Node previousNode = null;
        Node currentNode = first;
        while (currentNode != null) {
            Node nextNode = currentNode.next;
            currentNode.next = previousNode;
            previousNode = currentNode;
            currentNode = nextNode;
        }
        first = previousNode;
    }
}
