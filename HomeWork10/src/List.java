public interface List extends Collection {
    int get(int index);
    int indexOf(int element);
    boolean removeByIndex(int index);
    void insert(int element, int index);
    void reverse();
}
