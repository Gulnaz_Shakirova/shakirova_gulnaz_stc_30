import sun.security.util.ArrayUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collector;

public class Main {

    public static void main(String[] args) {
        int [] numberSequence = {120, 347, 8706, 4506, 77, 93};
        String[] processSequence = new String[] {"При23ве5т", "ка38к", "де5ла"};

        System.out.println(Arrays.toString(numberSequence));
        System.out.println(Arrays.toString(processSequence));

        NumbersAndStringProcessor numbersAndStringProcessor = new NumbersAndStringProcessor(processSequence, numberSequence);

        StringsProcess reverseString = new StringsProcess() {
            @Override
            public String process(String process) {
                return new StringBuilder(process).reverse().toString();
            }
        };
        StringsProcess replaceOfNumbers = process -> process.replaceAll("[0-9]", "");
        StringsProcess bigWords = process -> process.toUpperCase();

        NumbersProcess reverseOfNumbers = new NumbersProcess() {
            @Override
            public int process(int number) {
                int reverse = 0;
                while (number != 0) {
                    reverse = reverse * 10;
                    reverse = reverse + number % 10;
                    number = number / 10;
                }
                return reverse;
            }
        };

        NumbersProcess replaceOfNull = number -> {
            if (number != 0) {
                return Integer.parseInt(Integer.toString(number).replace("0", ""));
            }
            return 0;
        };

        NumbersProcess evenNumberReplacement = number -> {
            if (number %2 == 0) {
                number = number - 1;
            }
            return number;
        };

        System.out.println(Arrays.toString(numbersAndStringProcessor.process(reverseString)));
        System.out.println(Arrays.toString(numbersAndStringProcessor.process(replaceOfNumbers)));
        System.out.println(Arrays.toString(numbersAndStringProcessor.process(bigWords)));

        System.out.println(Arrays.toString(numbersAndStringProcessor.process(reverseOfNumbers)));
        System.out.println(Arrays.toString(numbersAndStringProcessor.process(replaceOfNull)));
        System.out.println(Arrays.toString(numbersAndStringProcessor.process(evenNumberReplacement)));
    }
}
