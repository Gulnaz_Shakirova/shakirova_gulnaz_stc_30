import java.util.Arrays;

public class NumbersAndStringProcessor {
    private int[] numberSequence;
    private String[] stringSequence;

    public NumbersAndStringProcessor (String[] stringSequence, int[] numberSequence) {
    this.stringSequence = stringSequence;
    this.numberSequence = numberSequence;
    }

    public String[] process (StringsProcess process) {
        String[] resultsString = new String[stringSequence.length];
        for (int i = 0; i < stringSequence.length; i++) {
            resultsString[i] = process.process(stringSequence[i]);
        }
        return resultsString;
    }

     public int[] process(NumbersProcess process) {
        int[] resultsNumber = new int[numberSequence.length];
        for (int i = 0; i < this.numberSequence.length; i++) {
             resultsNumber[i] = process.process(numberSequence[i]);
         }
        return resultsNumber;
    }
}
