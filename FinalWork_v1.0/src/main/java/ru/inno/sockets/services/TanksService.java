package ru.inno.sockets.services;

public interface TanksService {

        Long startGame(String firstPlayerName, String secondPlayerName, String firstPlayerIp, String secondPlayerIp);

        void shot(Long idOfGame, String shooterName, String targetName);

    }

