package ru.inno.sockets.services;

import ru.inno.sockets.models.Game;
import ru.inno.sockets.models.Player;
import ru.inno.sockets.models.Shot;
import ru.inno.sockets.repositories.GamesRepository;
import ru.inno.sockets.repositories.PlayersRepository;
import ru.inno.sockets.repositories.ShotsRepository;

import java.sql.Date;
import java.time.LocalDate;

public class TanksServiceImpl implements TanksService {

    private ShotsRepository shotsRepository;
    private PlayersRepository playersRepository;
    private GamesRepository gamesRepository;

    public TanksServiceImpl(ShotsRepository shotsRepository, PlayersRepository playersRepository,
                            GamesRepository gamesRepository) {
        this.shotsRepository = shotsRepository;
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
    }

    public Long startGame(String firstPlayerName, String secondPlayerName, String firstPlayerIp, String secondPlayerIp) {
        Player firstPlayer = playersRepository.findByName(firstPlayerName);
        Player secondPlayer = playersRepository.findByName(secondPlayerName);
        // если пользователи новые
        if (firstPlayer == null) {
            Player newPlayer = new Player(firstPlayerIp, firstPlayerName, 0, 0, 0);
  //            firstPlayer = new Player(firstPlayerName);
            playersRepository.save(newPlayer);
        } else {
            firstPlayer.setIp(firstPlayerIp);
            playersRepository.update(firstPlayer);
        }

        if (secondPlayer == null) {
            Player newPlayer = new Player(secondPlayerIp, secondPlayerName, 0, 0, 0);
//            secondPlayer = new Player(secondPlayerName);
            playersRepository.save(newPlayer);
        }else {
            secondPlayer.setIp(secondPlayerIp);
            playersRepository.update(secondPlayer);
        }

        Game game = new Game();
        game.setFirst(firstPlayer.getIdOfPlayer());
        game.setSecond(secondPlayer.getIdOfPlayer());
        game.setGameDate(Date.valueOf(LocalDate.now().toString()));

        gamesRepository.save(game);
        return game.getIdOfGame();
    }

    public void shot(Long idOfGame, String shooterName, String targetName) {
        Player shooter = playersRepository.findByName(shooterName);
        Player target = playersRepository.findByName(targetName);
        Game game = gamesRepository.findById(idOfGame);
        Shot shot = new Shot(shooter.getIdOfPlayer(), target.getIdOfPlayer(), idOfGame);
        if (game.getFirst().equals(shooter)) {
            game.setFirstPlayerShotsCount(game.getFirstPlayerShotsCount() + 1);
        } else if (game.getSecond().equals(shooter)) {
            game.setSecondPlayerShotsCount(game.getSecondPlayerShotsCount() + 1);
        }
        shotsRepository.save(shot);
        gamesRepository.update(game);
        System.out.println(" shots: " +
                game.getFirstPlayerShotsCount() + "|" + game.getSecondPlayerShotsCount());
    }
}
