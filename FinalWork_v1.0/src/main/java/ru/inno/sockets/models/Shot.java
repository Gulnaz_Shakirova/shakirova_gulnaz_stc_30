package ru.inno.sockets.models;

import lombok.*;

import java.sql.Time;

@Data
@AllArgsConstructor
@Builder

public class Shot {
    private Time timeOfShot;
    private String hitPlayer;
    private Long shooter;
    private Long target;
    private Long game;

    public Long shooter() {
        return shooter;
    }

    public Long target() {
        return target;
    }

    public Long game() {
        return game;
    }

    public Shot(Long shooter, Long target, Long game) {
        this.shooter = shooter;
        this.target = target;
        this.game = game;
    }
}
