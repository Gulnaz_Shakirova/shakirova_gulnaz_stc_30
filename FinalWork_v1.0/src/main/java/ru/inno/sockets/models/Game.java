package ru.inno.sockets.models;

import lombok.*;

import java.sql.Date;

@Data
@AllArgsConstructor
@Builder

public class Game {
    private Long idOfGame;
    private Date gameDate;
    private Long first;
    private Long second;
    private Integer firstPlayerShotsCount = 0;
    private Integer secondPlayerShotsCount = 0;
    private Integer durationOfGameInMinutes = 0;

    public Game() {
    }

    public Long getIdOfGame() {
        return idOfGame;
    }

    public void setIdOfGame(Long idOfGame) {
        this.idOfGame = idOfGame;
    }

    public Date getGameDate() {
        return gameDate;
    }

    public void setGameDate(Date gameDate) {
        this.gameDate = gameDate;
    }

    public Long getFirst() {
        return first;
    }

    public void setFirst(Long first) {
        this.first = first;
    }

    public Long getSecond() {
        return second;
    }

    public void setSecond(Long second) {
        this.second = second;
    }

    public Integer getFirstPlayerShotsCount() {
        return firstPlayerShotsCount;
    }

    public void setFirstPlayerShotsCount(Integer firstPlayerShotsCount) {
        this.firstPlayerShotsCount = firstPlayerShotsCount;
    }

    public Integer getSecondPlayerShotsCount() {
        return secondPlayerShotsCount;
    }

    public void setSecondPlayerShotsCount(Integer secondPlayerShotsCount) {
        this.secondPlayerShotsCount = secondPlayerShotsCount;
    }

    public Integer getDurationOfGameInMinutes() {
        return durationOfGameInMinutes;
    }

    public void setDurationOfGameInMinutes(Integer durationOfGameInMinutes) {
        this.durationOfGameInMinutes = durationOfGameInMinutes;
    }
}
