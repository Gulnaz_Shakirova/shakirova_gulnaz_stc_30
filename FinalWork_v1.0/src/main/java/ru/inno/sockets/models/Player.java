package ru.inno.sockets.models;

import lombok.*;

import java.util.Objects;

@Data
@AllArgsConstructor
@Builder

public class Player {

    private Long idOfPlayer;

    public Player(String ip, String name, Integer maxNumberOfPoints, Integer numberOfWins, Integer numberOfLesions) {
        this.ip = ip;
        this.name = name;
        this.maxNumberOfPoints = maxNumberOfPoints;
        this.numberOfWins = numberOfWins;
        this.numberOfLesions = numberOfLesions;
    }

    private String ip;
    private String name;
    private Integer maxNumberOfPoints;
    private Integer numberOfWins;
    private Integer numberOfLesions;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return Objects.equals(name, player.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    public Long getIdOfPlayer() {
        return idOfPlayer;
    }

}
