package ru.inno.sockets.server;

import ru.inno.sockets.services.TanksService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class GameServer {

    private Socket firstPlayer;
    private Socket secondPlayer;
    private Long idOfGame;
    private PlayerThread firstPlayerThread;
    private PlayerThread secondPlayerThread;

    private TanksService tanksService;

    private boolean isGameStarted = false;

    public GameServer(TanksService tanksService) {
        this.tanksService = tanksService;
    }

    public void start(int port) {
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            while (!isGameStarted) {
                if (firstPlayer == null) {
                    firstPlayer = serverSocket.accept();
                    String firstPlayerThreadIp = firstPlayer.getInetAddress().getHostAddress();
                    System.out.println("Первый игрок подключен");
                    firstPlayerThread = new PlayerThread(firstPlayer);
                    firstPlayerThread.playerValue = "PLAYER_1";
                    firstPlayerThread.ip = firstPlayerThreadIp;
                    firstPlayerThread.start();
                } else {
                    secondPlayer = serverSocket.accept();
                    String secondPlayerThreadIp = secondPlayer.getInetAddress().getHostAddress();
                    System.out.println("Второй игрок подключен");
                    secondPlayerThread = new PlayerThread(secondPlayer);
                    secondPlayerThread.playerValue = "PLAYER_2";
                    secondPlayerThread.ip = secondPlayerThreadIp;
                    secondPlayerThread.start();
                    isGameStarted = true;
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private class PlayerThread extends Thread {
        private Socket player;
        private String name;
        private String playerValue;
        private String ip;
        private BufferedReader fromClient;
        private PrintWriter toClient;

        public PlayerThread(Socket player) {
            this.player = player;
            try {
                this.fromClient = new BufferedReader(new InputStreamReader(player.getInputStream()));
                this.toClient = new PrintWriter(player.getOutputStream(), true);
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }

        @Override
        public void run() {
            while (true) {
                try {
                    String messageFromClient = fromClient.readLine();
                    if (messageFromClient != null) {
                        System.out.println("Получили сообщение от " +
                                player.getInetAddress().getHostAddress() + ":"
                                + player.getPort() + " - " + messageFromClient);

                        String command[] = messageFromClient.split(" ");

                        if (command[0].equals("start")) {
                            this.name = command[1];
                            if (firstPlayerThread.name != null && secondPlayerThread.name != null) {
                                idOfGame = tanksService.startGame(firstPlayerThread.name,
                                        secondPlayerThread.name, firstPlayerThread.ip, secondPlayerThread.ip);
                                firstPlayerThread.toClient.println("PLAYER_1");
                                secondPlayerThread.toClient.println("PLAYER_2");
                            }
                        } else if (command[0].equals("shot")) {
                            if (command[1].equals("PLAYER_1")) {
                                tanksService.shot(idOfGame, firstPlayerThread.name,
                                        secondPlayerThread.name);
                            } else if (command[1].equals("PLAYER_2")) {
                                tanksService.shot(idOfGame, secondPlayerThread.name,
                                        firstPlayerThread.name);
                            }
                        }

                        firstPlayerThread.toClient.println(messageFromClient);
                        secondPlayerThread.toClient.println(messageFromClient);
                    }
                } catch (IOException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }
    }
}
