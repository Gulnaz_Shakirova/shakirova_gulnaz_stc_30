package ru.inno.sockets.repositories;

import ru.inno.sockets.models.Shot;

import javax.sql.DataSource;
import java.sql.*;

public class ShotsRepositoryImpl implements ShotsRepository {

    // language=SQL
    private static final String SQL_INSERT = "insert into shot (time_of_shot, hit_a_player, " +
            "who_fired_id, who_was_shot_at_id, number_of_game) values (?, ?, ?, ?, ?);";

    private DataSource dataSource;

    public ShotsRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Shot entity) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT,
                     Statement.RETURN_GENERATED_KEYS);) {
            statement.setTime(1, entity.getTimeOfShot());
            statement.setString(2, entity.getHitPlayer());
            statement.setLong(3, entity.getShooter());
            statement.setLong(4, entity.getTarget());
            statement.setLong(5, entity.getGame());

            int insertedRowsCount = statement.executeUpdate();

            if (insertedRowsCount == 0) {
                throw new SQLException("Cannot save entity");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Shot entity) {
    }
}
