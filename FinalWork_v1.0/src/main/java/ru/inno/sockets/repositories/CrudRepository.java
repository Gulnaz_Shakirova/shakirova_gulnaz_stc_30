package ru.inno.sockets.repositories;

public interface CrudRepository<T, ID> {
    void save (T entity);
    void update (T entity);
}
