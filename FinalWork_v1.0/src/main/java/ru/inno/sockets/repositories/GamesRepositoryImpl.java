package ru.inno.sockets.repositories;

import ru.inno.sockets.models.Game;

import javax.sql.DataSource;
import java.sql.*;


public class GamesRepositoryImpl implements GamesRepository {
    // language=SQL
    private static final String SQL_INSERT = "insert into game (date_of_game, gamer_id_1, " +
            "gamer_id_2, number_of_shots_1_player, number_of_shots_2_player, " +
            "duration_of_game_in_minutes) values (?, ?, ?, ?, ?, ?);";

    // language=SQL
    private static final String SQL_UPDATE = "update game set " +
            "date_of_game = ?, " +
            "gamer_id_1 = ?, " +
            "gamer_id_2 = ?, " +
            "number_of_shots_1_player = ?, " +
            "number_of_shots_2_player = ?, " +
            "duration_of_game_in_minutes = ? where id_of_game = ?";

    // language=SQL
    private static final String SQL_FIND_BY_ID = "select * from game where id_of_game = ?";

    private DataSource dataSource;

    public GamesRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private RowMapper<Game> gameRowMapper = row ->
            Game.builder()
                    .idOfGame((long) row.getInt("id_of_game"))
                    .gameDate((Date) row.getDate("date_of_game"))
                    .first((long) row.getInt("gamer_id_1"))
                    .second((long) row.getInt("gamer_id_2"))
                    .firstPlayerShotsCount(row.getInt("number_of_shots_1_player"))
                    .secondPlayerShotsCount(row.getInt("number_of_shots_2_player"))
                    .durationOfGameInMinutes(row.getInt("duration_of_game_in_minutes"))
                    .build();

    @Override
    public void save(Game entity) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT,
                     Statement.RETURN_GENERATED_KEYS);) {

            System.out.println(entity.getGameDate());
            System.out.println(entity.getFirst());
            System.out.println(entity.getSecond());
            System.out.println(entity.getFirstPlayerShotsCount());
            System.out.println(entity.getSecondPlayerShotsCount());
            System.out.println(entity.getDurationOfGameInMinutes() + "www");

            statement.setDate(1, (Date) entity.getGameDate());
            statement.setLong(2, entity.getFirst());
            statement.setLong(3, entity.getSecond());
            statement.setInt(4, entity.getFirstPlayerShotsCount());
            statement.setInt(5, entity.getSecondPlayerShotsCount());
            statement.setInt(6, entity.getDurationOfGameInMinutes());

            int insertedRowsCount = statement.executeUpdate();

            if (insertedRowsCount == 0) {
                throw new SQLException("Cannot save entity");
            }
            ResultSet generatedIds = statement.getGeneratedKeys();
            if (generatedIds.next()) {
                int generatedId = generatedIds.getInt("id_of_game");
                entity.setIdOfGame((long) generatedId);
            } else {
                throw new SQLException("Cannot return id");
            }
            generatedIds.close();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Game entity) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE)) {
            statement.setDate(1, (Date) entity.getGameDate());
            statement.setLong(2, entity.getFirst());
            statement.setLong(3, entity.getSecond());
            statement.setInt(4, entity.getFirstPlayerShotsCount());
            statement.setInt(5, entity.getSecondPlayerShotsCount());
            statement.setInt(6, entity.getDurationOfGameInMinutes());
            statement.setInt(7, entity.getIdOfGame().intValue());

            int insertedRowsCount = statement.executeUpdate();

            if(insertedRowsCount == 0) {
                throw new SQLException("Cannot update entity");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Game findById(Long id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID)) {
            statement.setInt(1, id.intValue());
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                return gameRowMapper.mapRow(result);
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
