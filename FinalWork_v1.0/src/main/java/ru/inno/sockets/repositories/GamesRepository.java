package ru.inno.sockets.repositories;

import ru.inno.sockets.models.Game;

public interface GamesRepository extends CrudRepository<Game, Long> {
    Game findById(Long id);
}