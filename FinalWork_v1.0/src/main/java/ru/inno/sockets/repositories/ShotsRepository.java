package ru.inno.sockets.repositories;

import ru.inno.sockets.models.Shot;

public interface ShotsRepository extends CrudRepository <Shot, Long> {
}
