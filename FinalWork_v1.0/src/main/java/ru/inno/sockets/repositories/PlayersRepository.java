package ru.inno.sockets.repositories;

import ru.inno.sockets.models.Player;

import java.util.Optional;

public interface PlayersRepository extends CrudRepository<Player, Long> {
    Optional<Player> findById(Long id);
    Player findByName(String name);
}
