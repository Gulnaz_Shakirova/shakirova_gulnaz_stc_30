package ru.inno.sockets.repositories;

import ru.inno.sockets.models.Player;

import javax.sql.DataSource;
import java.sql.*;
import java.util.*;


public class PlayersRepositoryImpl implements PlayersRepository {
    // language=SQL
    private static final String SQL_INSERT = "insert into player (ip, name, max_number_of_points, " +
            "number_of_wins, number_of_lesions) values (?, ?, ?, ?, ?);";

    // language=SQL
    private static final String SQL_UPDATE = "update player set " +
            "ip = ?, " +
            "name = ?, " +
            "max_number_of_points = ?, " +
            "number_of_wins = ?, " +
            "number_of_lesions = ? where id_of_player = ?";

    // language=SQL
    private static final String SQL_FIND_BY_ID = "select * from player where id_of_player = ?";

    // language=SQL
    private static final String SQL_FIND_BY_NAME = "select * from player where name = ?";

    private DataSource dataSource;

    public PlayersRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private RowMapper<Player> playerRowMapper = row ->
            Player.builder()
                .idOfPlayer((long) row.getInt("id_of_player"))
                .ip(row.getString("ip"))
                .name(row.getString("name"))
                .maxNumberOfPoints(row.getInt("max_number_of_points"))
                .numberOfWins(row.getInt("number_of_wins"))
                .numberOfLesions(row.getInt("number_of_lesions"))
                .build();

    @Override
    public void save(Player entity) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT,
                     Statement.RETURN_GENERATED_KEYS);) {
            statement.setString(1, entity.getIp());
            statement.setString(2, entity.getName());
            statement.setInt(3, entity.getMaxNumberOfPoints());
            statement.setInt(4, entity.getNumberOfWins());
            statement.setInt(5, entity.getNumberOfLesions());

            int insertedRowsCount = statement.executeUpdate();

            if (insertedRowsCount == 0) {
                throw new SQLException("Cannot save entity");
            }
            ResultSet generatedIds = statement.getGeneratedKeys();
            if (generatedIds.next()) {
                int generatedId = generatedIds.getInt("id_of_player");
                entity.setIdOfPlayer((long) generatedId);
            } else {
                throw new SQLException("Cannot return id");
            }
            generatedIds.close();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Player entity) {
        try (Connection connection = dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement(SQL_UPDATE)) {
            statement.setString(1, entity.getIp());
            statement.setString(2, entity.getName());
            statement.setInt(3, entity.getMaxNumberOfPoints());
            statement.setInt(4, entity.getNumberOfWins());
            statement.setInt(5, entity.getNumberOfLesions());
            statement.setInt(6,entity.getIdOfPlayer().intValue());

            int insertedRowsCount = statement.executeUpdate();

            if(insertedRowsCount == 0) {
                throw new SQLException("Cannot update entity");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Optional<Player> findById(Long id) {
        try (Connection connection = dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID)) {
            statement.setInt(1, id.intValue());
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                return Optional.of(playerRowMapper.mapRow(result));
            } else {
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Player findByName(String name) {
        try (Connection connection = dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_NAME)) {
            statement.setString(1, name);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                return playerRowMapper.mapRow(result);
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
